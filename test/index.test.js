const request = require('supertest');

const app = require('../app');

/**
 * Réalise un test sur la route home 
 */
describe('test get /home', function() {
  it('responds whitout text', function(done) {
      request(app)
          .get('/home')
          .expect(200, done);
  });
});
